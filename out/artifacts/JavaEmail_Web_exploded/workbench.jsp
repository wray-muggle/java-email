<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<html>--%>
<%--<head>--%>
<%--    <title>控制台</title>--%>
<%--    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/app.css">--%>
<%--    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/subpage.css">--%>
<%--</head>--%>
<%--<body >--%>
<%--    <h1 class="callout">工作台</h1>--%>
<%--    <div>--%>
<%--        <h2 style="margin: 100px 0;text-align: center;">登录成功！可自定义工作台显示内容。。</h2>--%>
<%--    </div>--%>
<%--</body>--%>
<%--</html>--%>
<html style="height: 100%">
<head>
    <meta charset="utf-8">
</head>
<body style="height: 100%; margin: 0">
<div id="container" style="height: 100%"></div>

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts@5.2.2/dist/echarts.min.js"></script>
<!-- Uncomment this line if you want to dataTool extension
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts@5.2.2/dist/extension/dataTool.min.js"></script>
-->
<!-- Uncomment this line if you want to use gl extension
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts-gl@2/dist/echarts-gl.min.js"></script>
-->
<!-- Uncomment this line if you want to echarts-stat extension
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts-stat@latest/dist/ecStat.min.js"></script>
-->
<!-- Uncomment this line if you want to use map
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts@5.2.2/map/js/china.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts@5.2.2/map/js/world.js"></script>
-->
<!-- Uncomment these two lines if you want to use bmap extension
<script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=<Your Key Here>"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/echarts@{{version}}/dist/extension/bmap.min.js"></script>
-->

<script type="text/javascript">
    var dom = document.getElementById("container");
    var myChart = echarts.init(dom);
    var app = {};

    var option;



    var uploadedDataURL = ROOT_PATH + '/data-gl/asset/data/flights.json';
    myChart.showLoading();
    $.getJSON(uploadedDataURL, function (data) {
        myChart.hideLoading();
        function getAirportCoord(idx) {
            return [data.airports[idx][3], data.airports[idx][4]];
        }
        var routes = data.routes.map(function (airline) {
            return [getAirportCoord(airline[1]), getAirportCoord(airline[2])];
        });
        myChart.setOption({
            geo3D: {
                map: 'world',
                shading: 'realistic',
                silent: true,
                environment: '#333',
                realisticMaterial: {
                    roughness: 0.8,
                    metalness: 0
                },
                postEffect: {
                    enable: true
                },
                groundPlane: {
                    show: false
                },
                light: {
                    main: {
                        intensity: 1,
                        alpha: 30
                    },
                    ambient: {
                        intensity: 0
                    }
                },
                viewControl: {
                    distance: 70,
                    alpha: 89,
                    panMouseButton: 'left',
                    rotateMouseButton: 'right'
                },
                itemStyle: {
                    color: '#000'
                },
                regionHeight: 0.5
            },
            series: [
                {
                    type: 'lines3D',
                    coordinateSystem: 'geo3D',
                    effect: {
                        show: true,
                        trailWidth: 1,
                        trailOpacity: 0.5,
                        trailLength: 0.2,
                        constantSpeed: 5
                    },
                    blendMode: 'lighter',
                    lineStyle: {
                        width: 0.2,
                        opacity: 0.05
                    },
                    data: routes
                }
            ]
        });
        window.addEventListener('keydown', function () {
            myChart.dispatchAction({
                type: 'lines3DToggleEffect',
                seriesIndex: 0
            });
        });
    });

    if (option && typeof option === 'object') {
        myChart.setOption(option);
    }

</script>
</body>
</html>
