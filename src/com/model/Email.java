package com.model;

import java.sql.Timestamp;

/**
 * @author Wray
 */
public class Email {
    public Integer getEmailId() {
        return emailId;
    }

    public void setEmailId(Integer emailId) {
        this.emailId = emailId;
    }

    private Integer emailId;
    /**
     * 邮件id
     */


    private String sender;

    private String receiver;


    private String title;

    private String text;

    private Timestamp time;

    /**
     * 状态，具体参考Action，3 双方都存在此邮件
     * 2 收方/收方删除邮件
     * 1 发方/发方删除邮件
     * 0 双方都删除此邮件，此时真正删除
     */
    private Integer status;


    @Override
    public String toString() {
        return "Email{" +
                "sender='" + sender + '\'' +
                ", receiver='" + receiver + '\'' +
                ", title='" + title + '\'' +
                ", text='" + text + '\'' +
                ", time=" + time +
                '}';
    }

    public Email() {
    }

    /**
     * mysql时间，邮件收/发时间
     */


    public Email(String sender, String receiver, String title, String text, Timestamp time, Integer status) {
        this.sender = sender;
        this.receiver = receiver;
        this.title = title;
        this.text = text;
        this.time = time;
        this.status = status;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
