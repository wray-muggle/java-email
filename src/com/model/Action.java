package com.model;


/**
 * 各命令
 * @author Wray
 */
public class Action {
    /**
     * 双方都存在此邮件
     */
    public static final Integer BOTH_EXIST=3;

    /**
     * 收方/收方删除邮件
     */
    public static final Integer RECEIVER=2;
    /**
     * 发方/发方删除邮件
     */
    public static final Integer SENDER=1;
    /**
     * 双方都删除此邮件
     */
    public static final Integer BOTH_DELETE=0;






}
