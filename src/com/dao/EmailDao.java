package com.dao;

import com.model.Action;
import com.model.Email;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author Wray
 */
public interface EmailDao {


    /**
     * 插入邮件
     * @param email 邮件
     * @return bool值
     */
    Boolean insertEmail(Email email);


    /**
     * 通过用户查找邮件
     * @param username 用户名
     * @param action 判断是发送者还是接收者
     * @return 邮件列表
     */
    List<Email> selectByUser(String username, Integer action);

    /**
     * 通过邮件id来查找邮件，用于删除时
     * @param emailId 邮件id
     * @return 邮件
     */
    Email selectByEmailId(Integer emailId);

    /**
     * 通过邮件id删除邮件
     * @param emailId 邮件id
     * @param action 判断是发送者还是接收者
     * @return bool值
     */
    Boolean deleteEmail(Integer emailId, Integer action);

}
