package com.dao;

import com.model.User;

/**
 * @author Wray
 */
public interface UserDao {

    /**
     * 通过用户名查用户
     * @param username
     * @return
     */
    public User selectByUsername(String username);

    /**
     * 注册
     * @param username 用户名
     * @param password 密码
     * @return bool值
     */
    public Boolean register(String username,String password);

    /**
     * 销户
     * @param username 用户名
     * @return bool值
     */
    public Boolean delete(String username);

    /**
     * 更新信息
     * @param user 用户
     * @return bool值
     */
    public Boolean update(User user);
}
