package com.dao.implement;

import com.dao.EmailDao;
import com.model.Action;
import com.model.Email;
import com.utils.JdbcUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Wray
 */
public class EmailDaoImpl implements EmailDao {


    /**
     * 插入邮件
     * @param email 邮件
     * @return bool值
     */
    @Override
    public Boolean insertEmail( Email email) {
        String sql="insert into emails(sender,receiver,title,text,time,status) values(?,?,?,?,?,?)";
        Boolean flag=false;
        try (Connection conn = JdbcUtil.getConnection(); PreparedStatement pstmt = conn.prepareStatement(sql)){
            pstmt.setString(1,email.getSender());
            pstmt.setString(2,email.getReceiver());
            pstmt.setString(3,email.getTitle());
            pstmt.setString(4, email.getText());
            pstmt.setTimestamp(5,email.getTime());
            pstmt.setInt(6,Action.BOTH_EXIST);
            flag=(pstmt.executeUpdate()!=0);
        }catch (SQLException e){
            e.printStackTrace();
        }
        return flag;
    }

    /**
     *通过emailId删除邮件
     * @param emailId 邮件id
     * @param action 判断是发送者还是接收者
     */
    @Override
    public Boolean deleteEmail(Integer emailId, Integer action) {
        Boolean flag=null;
        Email email=selectByEmailId(emailId);
        PreparedStatement pstmt = null;
        try (Connection conn = JdbcUtil.getConnection(); ){

            String sql;
            if(email.getStatus().equals(Action.BOTH_EXIST)) {
                sql = "update emails set status = ? where emailId = ?;";
                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1,action);
                pstmt.setInt(2,emailId);
            }else{
                sql="delete from emails where emailId = ?;";
                pstmt = conn.prepareStatement(sql);
                pstmt.setInt(1,emailId);
            }

           flag=(pstmt.executeUpdate()>0);
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            try {
                pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return flag;
    }


    /**
     * 通过邮件id来查找邮件，用于删除时
     * @param emailId 邮件id
     * @return 邮件
     */
    @Override
    public Email selectByEmailId(Integer emailId) {


        Email email=new Email();
        String sql="select * from emails where emailId=?";
        ResultSet rs=null;
        try (Connection conn = JdbcUtil.getConnection(); PreparedStatement pstmt = conn.prepareStatement(sql); ){

                pstmt.setInt(1,emailId);
                rs=pstmt.executeQuery();

                while (rs.next()) {
                    email=new Email(rs.getString("sender"), rs.getString("receiver"),rs.getString("title"),rs.getString("text"),rs.getTimestamp("time"),rs.getInt("status") );
                }


        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return email;
    }

    /**
     * 通过用户查找邮件
     * @param username 用户名
     * @param action 判断是发送者还是接收者
     * @return 邮件列表
     */
    @Override
    public List<Email> selectByUser(String username, Integer action) {
        List<Email> emailList=new ArrayList<>();
        String sql=null;
        if(action.equals(Action.SENDER)){
        sql= "select * from emails where sender =? order by time desc";
        } else{
            sql="select * from emails where receiver =? order by time desc";
        }

        ResultSet rs=null;

        try (Connection conn = JdbcUtil.getConnection(); PreparedStatement pstmt = conn.prepareStatement(sql); ){
                pstmt.setString(1,username);
                rs=pstmt.executeQuery();
                while (rs.next()) {
                    Email email=new Email();
                    email.setEmailId(rs.getInt("emailId"));
                    email.setSender(rs.getString("sender"));
                    email.setReceiver(rs.getString("receiver"));
                    email.setTitle(rs.getString("title"));
                    email.setText(rs.getString("text"));
                    email.setTime(rs.getTimestamp("time"));
                    email.setStatus(rs.getInt("status"));
                    if(!email.getStatus().equals(action)&&!email.getStatus().equals(Action.BOTH_DELETE)){
                        emailList.add(email);
                    }
                }

    }catch (SQLException e){
            e.printStackTrace();
        }finally {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return emailList;
    }
}
