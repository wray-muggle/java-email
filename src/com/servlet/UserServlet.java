package com.servlet;


import com.dao.UserDao;
import com.dao.implement.UserDaoImpl;
import com.model.User;
import com.utils.Md5Util;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UserServlet
 * @author WANGZIC
 */
@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    private UserDao dao = new UserDaoImpl();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=utf-8");
        String act =request.getParameter("act");
        if(act!=null){
            //根据传输的act调用不同的方法进行处理
            switch (act) {
                case "toUpdatePage":
                    toUpdatePage(request,response);
                    break;
                case "select":
                    selectUser(request,response);
                    break;
//                case "insert":
//                    insertUser(request,response);
//                    break;
                case "update":
                    updateUser(request, response);
                    break;
                case "delete":
                    deleteUser(request, response);
                    break;
                default:selectUser(request,response);
                    break;
            }
        }else {
            response.getWriter().print("act参数不能位空，请检查是否传输了该参数！");
        }

    }

    private void toUpdatePage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException  {
        String username =request.getParameter("username");
        User user = dao.selectByUsername(username);
        request.setAttribute("updateUser", user);
        request.getRequestDispatcher("pages/usermgr/update.jsp").forward(request, response);
    }

    private void selectUser(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = (String)request.getSession().getAttribute("username");
        User user = dao.selectByUsername(username);
        request.setAttribute("user", user);
        request.getRequestDispatcher("pages/usermgr/index.jsp").forward(request, response);
    }

//    private void insertUser(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
//        String username = request.getParameter("username");
//        String nickname = request.getParameter("nickname");
//        String password = request.getParameter("password");
//        String phone = request.getParameter("phone");
//        String gender = request.getParameter("gender");
//        String email = request.getParameter("email");
//        String address = request.getParameter("address");
//        User user = new User();
//        if(username!=null){
//            user.setUsername(username);
//        }
//        if(nickname!=null){
//            user.setNickname(nickname);
//        }
//        if(password!=null){
//            user.setPassword(password);
//        }
//
//        if(dao.register(user)){
//            request.getSession().setAttribute("resultMSG", "新增成功");
//            response.sendRedirect("UserServlet?act=select");
//        }else {
//            request.getSession().setAttribute("resultMSG", "新增失败");
//            request.getRequestDispatcher("pages/usermgr/insert.jsp").forward(request, response);
//        }
//    }

    private void deleteUser(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
          String username =request.getParameter("username");
        if(dao.delete(username)){
            request.getSession().setAttribute("resultMSG", "删除成功");
            request.getSession().invalidate();
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }else {
            request.getSession().setAttribute("resultMSG", "删除失败");
            response.sendRedirect("UserServlet?act=select");
        }
    }

    private void updateUser(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
        String username = request.getParameter("username");
        String nickname = request.getParameter("nickname");
        String password = request.getParameter("password");

        if (username == null || username.trim().isEmpty()) {
            request.setAttribute("registError", "用户名不能为空");
            request.getRequestDispatcher("pages/usermgr/update.jsp").forward(request, response);
            return;
        }
        if (nickname == null || nickname.trim().isEmpty()) {
            request.setAttribute("updateError", "昵称不能为空");
            request.getRequestDispatcher("pages/usermgr/update.jsp").forward(request, response);
            return;
        }
        if (password == null || password.trim().isEmpty()) {
            request.setAttribute("updateError", "密码不能为空");
            request.getRequestDispatcher("pages/usermgr/update.jsp").forward(request, response);
            return;
        }
        User user = new UserDaoImpl().selectByUsername(username);
        user.setNickname(nickname);
        if(!password.equals(user.getPassword())){
            user.setPassword(Md5Util.getmd5Str(password));
        }

        if (dao.update(user)) {
            request.getSession().setAttribute("user",user);
            request.getSession().setAttribute("resultMSG", "修改成功");
            request.getRequestDispatcher("pages/usermgr/index.jsp").forward(request, response);
        } else {
            request.setAttribute("updateUser", user);
            request.getSession().setAttribute("resultMSG", "修改失败");
            request.getRequestDispatcher("pages/usermgr/update.jsp").forward(request, response);
        }
    }
}
