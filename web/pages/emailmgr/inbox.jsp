<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" import="java.sql.*"%>
<%@ page import="com.model.User" %>
<%@ page import="java.awt.*" %>
<%@ page import="java.util.List" %>
<%@ page import="com.model.Email" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/app.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/subpage.css">
</head>
<body>
<h1 class="callout">收件箱</h1>
<%--    <h3><a href="${pageContext.request.contextPath}/pages/usermgr/insert.jsp" class="wzc-btn">修改信息</a></h3>--%>
<div class="" style="text-align:center;">
    <table class="wzc-table">
        <tr>
<%--            <th type="hidden"></th>--%>
            <th>发件人</th>
            <th>标题</th>
            <th>文本</th>
            <th>时间</th>
            <%--                <th>密码</th>--%>
            <th>操作</th>
        </tr>
        <%
            String username=  request.getParameter("username");
            List<Email> emailList= (List<Email>) request.getAttribute("emailList");
            if(emailList==null){

            }else{for (Email email : emailList) {

        %>
        <tr>
<%--            <td type="hidden"><%=email.getEmailId()%>></td>--%>
            <td><%=email.getSender()%></td>
            <td><%=email.getTitle()%></td>
            <td><%=email.getText()%></td>
            <td><%=email.getTime()%></td>
            <%--                <td><%=user.getPassword()==null?"":user.getPassword()%></td>--%>

            <td>
                <a href="${pageContext.request.contextPath}/EmailServlet?act=deleteByReceiver&emailId=<%=email.getEmailId()%>&username=<%=email.getReceiver()%>">
                    <i class="fa fa-trash fa-fw"></i> 删除</a>
            </td>
        </tr>
        <%}} %>
    </table>
<%--    <%--%>
<%--        Object resultMSG = request.getSession().getAttribute("resultMSG");--%>
<%--        if(resultMSG!=null){--%>
<%--    %><script type="text/javascript">alert("<%=resultMSG.toString() %>")</script><%--%>
<%--        request.getSession().removeAttribute("resultMSG");--%>
<%--        response.sendRedirect("login.jsp");--%>
<%--    }--%>
<%--%>--%>
</div>
</body>
</html>
