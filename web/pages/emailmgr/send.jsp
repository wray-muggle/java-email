<%@ page import="com.model.User" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/app.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/subpage.css">
</head>
<body>
<%
    User user = (User)request.getAttribute("user");
%>
<h1 class="callout">发送邮件</h1>
<div class="">
    <form action="${pageContext.request.contextPath}/EmailServlet?act=sendEmail" method="post" style="width:100%;text-align:center;">
        <table class="wzc-table">
            <tr>
                <th>发件人</th>
                <td><span>${sessionScope.user.username }</span></td>
            </tr>
            <tr>
                <th>收件人</th>
                <td><input name="receivername" type="text" ></td>
            </tr>
            <tr>
                            <td colspan="2">
                                <font color="red" size="2">${receiverError }</font>
                            </td>
            </tr>
            <tr>
                <th>标题</th>
                <td><input name="title" type="text" ></td>
            </tr>
            <tr>
                                <td colspan="2">
                                    <font color="red" size="2">${titleError }</font>
                                </td>
            </tr>
            <tr>
                <th>文本</th>
                <td><input name="text" type="text" ></td>

            </tr>
            <tr>
                                <td colspan="2">
                                    <font color="red" size="2">${textError }</font>
                                </td>
            </tr>
            <tr>
<%--                <td colspan="2">--%>
<%--                    <font color="red" size="2">${sendError }</font>--%>
<%--                </td>--%>
            </tr>
            <tr>
                <td colspan='2'><input name="sub" type="submit" value="发送" class="wzc-btn" style="width: 200px;"></td>
            </tr>
            <tr>
                <td colspan='2'><a href="${pageContext.request.contextPath}/UserServlet?act=select" class="wzc-btn" style="width: 200px;">返回</a></td>
            </tr>
        </table>
    </form>
</div>

<%
    Object resultMSG = request.getSession().getAttribute("resultMSG");
    if(resultMSG!=null){
%><script type="text/javascript">alert("<%=resultMSG.toString() %>")</script><%
        request.getSession().removeAttribute("resultMSG");
    }
%>
</body>
</html>