<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" import="java.sql.*"%>
<%@ page import="com.model.User" %>
<%@ page import="java.awt.*" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <title></title>
    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/app.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/subpage.css">
</head>
<body>
    <h1 class="callout">用户管理</h1>
<%--    <h3><a href="${pageContext.request.contextPath}/pages/usermgr/insert.jsp" class="wzc-btn">修改信息</a></h3>--%>
    <div class="" style="text-align:center;">
        <table class="wzc-table">
            <tr>

                <th>用户名</th>
                <th>昵称</th>
<%--                <th>密码</th>--%>
                <th>操作</th>
            </tr>
            <%
//                List<User> uList = (List<User>)request.getAttribute("userList");
                User user=(User)request.getSession().getAttribute("user");
                if(user==null){
                    request.getRequestDispatcher("login.jsp").forward(request, response);
                }else{
            %>
            <tr>

                <td><%=user.getUsername()==null?"":user.getUsername()%></td>
                <td><%=user.getNickname()==null?"":user.getNickname()%></td>
<%--                <td><%=user.getPassword()==null?"":user.getPassword()%></td>--%>
                <td><a href="${pageContext.request.contextPath}/UserServlet?act=toUpdatePage&username=<%=user.getUsername()%>" style="margin:0 10px;">
                    <i class="fa fa-edit fa-fw"></i> 修改</a>
                    <a href="${pageContext.request.contextPath}/UserServlet?act=delete&username=<%=user.getUsername()%>" target="_blank">
                        <i class="fa fa-trash fa-fw"></i> 注销</a>
                </td>
            </tr>
            <%} %>
        </table>
    </div>
    <%
        Object resultMSG = request.getSession().getAttribute("resultMSG");
        if(resultMSG!=null){
    %><script type="text/javascript">alert("<%=resultMSG.toString() %>")</script><%
            request.getSession().removeAttribute("resultMSG");
        }
    %>
</body>
</html>
