<%@ page import="com.model.User" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/app.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/static/css/subpage.css">
</head>
<body>
<%
	User user = (User)request.getAttribute("updateUser");
%>
<h1 class="callout">用户信息修改</h1>
<div>
	<form action="${pageContext.request.contextPath}/UserServlet?act=update" method="post" style="width:100%;text-align:center;">
		<input name="username" type="hidden" value='<%=user.getUsername() %>'>
        <table class="wzc-table">
			<tr>
                <th>用户名</th>
<%--                <td><input name="username" type="text" readonly value='<%=user.getUsername()==null?"":user.getUsername()%>'></td>--%>
                <td><span>${sessionScope.user.username }</span></td>
            </tr>
            <tr>
                <th>昵称</th>
                <td><input name="nickname" type="text" value='<%=user.getNickname()==null?"":user.getNickname()%>'></td>
            </tr>
            <tr>
                <th>密码</th>
                <td><input name="password" type="password" value='<%=user.getPassword()==null?"":user.getPassword()%>'></td>
            </tr>
            <tr>
                <td><input name="sub" type="submit" value="修改" class="wzc-btn" style="width: 200px;"></td>
                <td><a href="${pageContext.request.contextPath}/UserServlet?act=select" class="wzc-btn" style="width: 200px;">返回</a></td>
            </tr>
		</table>
	</form>
</div>
<%
	Object resultMSG = request.getSession().getAttribute("resultMSG");
	if(resultMSG!=null){
%>
<script type="text/javascript">
    alert("<%=resultMSG.toString() %>")
</script>
<%
		request.getSession().removeAttribute("resultMSG");
	}
%>
<a href="${pageContext.request.contextPath}/LogoutServlet">
</a>
</body>
</html>